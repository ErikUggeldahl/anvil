extern crate anvil;

use anvil::{Augment, Card, CardIndex, Coord, Direction, FieldSpace, FieldSpaceState,
            GameState, Player, PlayerID, TerminalState};

use anvil::action::Action;
use anvil::available_actions::all_available_actions;
use anvil::mcts;

use std::io;

trait Reverse {
    fn reverse(self) -> Self;
}

impl Reverse for Coord {
    fn reverse(self) -> Coord {
        let Coord(x, y, z) = self;
        Coord(-x, -y, -z)
    }
}

impl Reverse for Direction {
    fn reverse(self) -> Direction {
        match self {
            Direction::NorthWest => Direction::SouthEast,
            Direction::NorthEast => Direction::SouthWest,
            Direction::West => Direction::East,
            Direction::East => Direction::West,
            Direction::SouthWest => Direction::NorthEast,
            Direction::SouthEast => Direction::NorthWest,
        }
    }
}

impl Reverse for Action {
    fn reverse(self) -> Action {
        match self {
            Action::Play(play_index, discard_indices, to) => {
                Action::Play(play_index, discard_indices, to.reverse())
            }
            Action::Move(from, direction) => Action::Move(from.reverse(), direction.reverse()),
            Action::Attack(with, direction, augment) => {
                Action::Attack(with.reverse(), direction.reverse(), augment)
            }
            _ => self,
        }
    }
}

fn clear_screen() {
    print!("\x1B[2J");
}

trait Render {
    fn render(&self, active_player: bool) -> String;
}

impl Render for PlayerID {
    fn render(&self, _: bool) -> String {
        match *self {
            PlayerID::Player1 => "1".to_string(),
            PlayerID::Player2 => "2".to_string(),
        }
    }
}

impl Render for Card {
    fn render(&self, active_player: bool) -> String {
        format!(
            "{}{}{}",
            if !active_player { "\x1B[91m" } else { "" },
            match self.augment {
                Augment::NoAugment => self.rank.to_string(),
                Augment::Small => "½".to_string(),
                Augment::Large => "⅔".to_string(),
            },
            if !active_player { "\x1B[39m" } else { "" }
        )
    }
}

impl Render for FieldSpace {
    fn render(&self, active_player: bool) -> String {
        match &self.state {
            &FieldSpaceState::Empty => "∙".to_string(),
            &FieldSpaceState::FaceDown(ref card) if active_player => {
                format!("{}{}{}", "\x1B[44m", card.render(true), "\x1B[49m")
            }
            &FieldSpaceState::FaceDown(_) => "\x1B[91m?\x1B[39m".to_string(),
            &FieldSpaceState::Ready(ref card)
            | &FieldSpaceState::Moved(ref card)
            | &FieldSpaceState::Attacked(ref card) => card.render(active_player),
        }
    }
}

impl Render for Coord {
    fn render(&self, _: bool) -> String {
        let &Coord(x, _, z) = self;
        let row = match z {
            3 => 'A',
            2 => 'B',
            1 => 'C',
            0 => 'D',
            -1 => 'E',
            -2 => 'F',
            -3 => 'G',
            _ => panic!(),
        };
        let column = if z < 0 { x + z + 3 } else { x + 3 };
        format!("{}{}", row, column)
    }
}

impl Render for Direction {
    fn render(&self, _: bool) -> String {
        match *self {
            Direction::NorthWest => "NW",
            Direction::NorthEast => "NE",
            Direction::West => "W",
            Direction::East => "E",
            Direction::SouthWest => "SW",
            Direction::SouthEast => "SE",
        }.to_string()
    }
}

impl Render for Player {
    fn render(&self, active_player: bool) -> String {
        fn render_optional_card(card: Option<&Card>) -> String {
            match card {
                Some(card) => card.render(true),
                None => " ".to_string(),
            }
        }

        let mut hand = self.hand.iter();
        if active_player {
            format!(
                "H: {} {} {} {} {} {} {} {}  ║\n\
                 ║Dk: {:<2}, Dc: {:<2}      ",
                render_optional_card(hand.next()),
                render_optional_card(hand.next()),
                render_optional_card(hand.next()),
                render_optional_card(hand.next()),
                render_optional_card(hand.next()),
                render_optional_card(hand.next()),
                render_optional_card(hand.next()),
                render_optional_card(hand.next()),
                self.deck.len(),
                self.discard_pile.len()
            )
        } else {
            format!(
                "H: {}, Dk: {:<2}, Dc: {:<2}",
                self.hand.len(),
                self.deck.len(),
                self.discard_pile.len()
            )
        }
    }
}

impl Render for GameState {
    fn render(&self, _: bool) -> String {
        let active_player = &self.current_player();
        let other_player = &self.other_player();

        let mut spaces = self.field.iter().collect::<Vec<_>>();
        spaces.sort_by(|&(a, _), &(b, _)| match a.2.cmp(&b.2) {
            std::cmp::Ordering::Equal => a.0.cmp(&b.0),
            ord => ord,
        });

        let spaces = spaces.into_iter().map(|(_, space)| space);
        let spaces = if self.player_to_act == PlayerID::Player1 {
            spaces.collect::<Vec<_>>()
        } else {
            spaces.rev().collect::<Vec<_>>()
        };

        fn space_is_active_player(space: &FieldSpace, active_player: PlayerID) -> bool {
            match &space.state {
                &FieldSpaceState::Ready(ref card)
                | &FieldSpaceState::Moved(ref card)
                | &FieldSpaceState::Attacked(ref card)
                | &FieldSpaceState::FaceDown(ref card) => card.owner == active_player,
                _ => true,
            }
        }

        let renders = spaces
            .iter()
            .map(|&space| space.render(space_is_active_player(&space, self.player_to_act)))
            .collect::<Vec<_>>();

        format!(
            "\
             ╔════════════════════╗\n\
             ║{}║\n\
             ╟────────────────────╢\n\
             ║        {} {}         ║\n\
             ║       {} {} {}        ║\n\
             ║      {} {} {} {}       ║\n\
             ║     {} {} {} {} {}      ║\n\
             ║      {} {} {} {}       ║\n\
             ║       {} {} {}        ║\n\
             ║        {} {}         ║\n\
             ╟────────────────────╢\n\
             ║{}║\n\
             ╚════════════════════╝",
            other_player.render(false),
            renders[0],
            renders[1],
            renders[2],
            renders[3],
            renders[4],
            renders[5],
            renders[6],
            renders[7],
            renders[8],
            renders[9],
            renders[10],
            renders[11],
            renders[12],
            renders[13],
            renders[14],
            renders[15],
            renders[16],
            renders[17],
            renders[18],
            renders[19],
            renders[20],
            renders[21],
            renders[22],
            active_player.render(true),
        )
    }
}

impl Render for Action {
    fn render(&self, _: bool) -> String {
        match *self {
            Action::Play(_, _, coord) => format!("Play to {}", coord.render(false)),
            Action::Move(coord, direction) => {
                format!("Move {} {}", coord.render(false), direction.render(false))
            }
            Action::Attack(coord, direction, augment) => format!(
                "Attack {} {} {}",
                coord.render(false),
                direction.render(false),
                if augment.is_some() {
                    "with augment"
                } else {
                    "without augment"
                }
            ),
            Action::Defend(augment, _) => format!(
                "Defend {}",
                if augment.is_some() {
                    "with augment"
                } else {
                    "without augment"
                }
            ),
            Action::End(_) => "End".to_string(),
        }
    }
}

type ActionLog = Vec<(PlayerID, Action)>;

impl Render for ActionLog {
    fn render(&self, _: bool) -> String {
        let mut render = String::new();
        render += &format!("\
            \x1B[s\
            \x1B[1;23H╔════════════════════════════════════╗\
            \x1B[2;23H║ Action Log                         ║\
            \x1B[3;23H╟────────────────────────────────────╢"
        );
        for (i, (action_number, &(player, ref action))) in
            self.iter().enumerate().rev().take(10).enumerate()
        {
            render += &format!(
                "\x1B[{};23H║{:<3} P{} {:<29}║",
                4 + i,
                action_number + 1,
                player.render(false),
                action.render(false),
            );
        }
        render += &format!("\
            \x1B[{};23H╚════════════════════════════════════╝\
            \x1B[u",
            std::cmp::min(self.len(), 10) + 4
        );
        render
    }
}

enum Command {
    Action(Action),
    Quit,
}

trait Parse
where
    Self: Sized,
{
    fn parse(user_input: &str) -> Result<Self, ()>;
}

impl Parse for Command {
    fn parse(command: &str) -> Result<Self, ()> {
        let mut parts = command.split_whitespace();
        let command = parts.next().ok_or(())?;
        match command {
            "p" | "play" => parse_play_action(&mut parts),
            "m" | "move" => parse_move_action(&mut parts),
            "a" | "attack" => parse_attack_action(&mut parts),
            "d" | "defend" => parse_defend_action(&mut parts),
            "e" | "end" => parse_end_action(&mut parts),
            "q" | "quit" => Ok(Command::Quit),
            _ => Err(()),
        }
    }
}

impl Parse for CardIndex {
    fn parse(index: &str) -> Result<Self, ()> {
        match index.parse::<CardIndex>() {
            Ok(index) => index.checked_sub(1).ok_or(()),
            _ => Err(()),
        }
    }
}

impl Parse for Option<CardIndex> {
    fn parse(index: &str) -> Result<Self, ()> {
        match index.parse::<CardIndex>() {
            Ok(index) if index == 0 => Ok(None),
            Ok(index) => index.checked_sub(1).map(|index| Some(index)).ok_or(()),
            _ => Err(()),
        }
    }
}

impl Parse for Vec<CardIndex> {
    fn parse(array: &str) -> Result<Self, ()> {
        array
            .split(',')
            .map(|index| CardIndex::parse(index))
            .collect::<Result<Vec<_>, _>>()
    }
}

impl Parse for Coord {
    fn parse(coord: &str) -> Result<Self, ()> {
        let mut chars = coord.chars();
        let row = chars.next().ok_or(())?;
        let z = match row {
            'a' => 3,
            'b' => 2,
            'c' => 1,
            'd' => 0,
            'e' => -1,
            'f' => -2,
            'g' => -3,
            _ => return Err(()),
        };
        let base = if z < 0 { z } else { 0 };
        let column = chars.next().ok_or(())?;
        let x = match column {
            '1' => -2 - base,
            '2' => -1 - base,
            '3' => 0 - base,
            '4' => 1 - base,
            '5' => 2 - base,
            _ => return Err(()),
        };
        Ok(Coord(x, -x - z, z))
    }
}

impl Parse for Direction {
    fn parse(direction: &str) -> Result<Self, ()> {
        Ok(match direction {
            "nw" => Direction::NorthWest,
            "ne" => Direction::NorthEast,
            "w" => Direction::West,
            "e" => Direction::East,
            "sw" => Direction::SouthWest,
            "se" => Direction::SouthEast,
            _ => return Err(()),
        })
    }
}

fn parse_play_action(parts: &mut Iterator<Item = &str>) -> Result<Command, ()> {
    let play_index = CardIndex::parse(parts.next().ok_or(())?)?;
    let discard_or_coord = parts.next().ok_or(())?;
    let discard_indices = match Coord::parse(discard_or_coord) {
        Ok(coord) => return Ok(Command::Action(Action::Play(play_index, Vec::new(), coord))),
        Err(_) => Vec::parse(discard_or_coord)?,
    };
    let coord = Coord::parse(parts.next().ok_or(())?)?;
    Ok(Command::Action(Action::Play(
        play_index,
        discard_indices,
        coord,
    )))
}

fn parse_move_action(parts: &mut Iterator<Item = &str>) -> Result<Command, ()> {
    let coord = Coord::parse(parts.next().ok_or(())?)?;
    let direction = Direction::parse(parts.next().ok_or(())?)?;
    Ok(Command::Action(Action::Move(coord, direction)))
}

fn parse_attack_action(parts: &mut Iterator<Item = &str>) -> Result<Command, ()> {
    let coord = Coord::parse(parts.next().ok_or(())?)?;
    let direction = Direction::parse(parts.next().ok_or(())?)?;
    let augment = match parts.next() {
        Some(index) => Some(CardIndex::parse(index)?),
        None => None,
    };
    Ok(Command::Action(Action::Attack(coord, direction, augment)))
}

fn parse_defend_action(parts: &mut Iterator<Item = &str>) -> Result<Command, ()> {
    let augment = match parts.next() {
        Some(index) => Option::parse(index)?,
        None => None,
    };
    let discard_indices = match parts.next() {
        Some(discard_indices) => Vec::parse(discard_indices)?,
        None => Vec::new(),
    };
    Ok(Command::Action(Action::Defend(augment, discard_indices)))
}

fn parse_end_action(parts: &mut Iterator<Item = &str>) -> Result<Command, ()> {
    let discard_indices = match parts.next() {
        Some(indices) => Vec::parse(indices)?,
        None => Vec::new(),
    };
    Ok(Command::Action(Action::End(discard_indices)))
}

fn main() {
    let mut game = GameState::new().init();
    let mut message_log = Vec::<String>::new();
    let mut action_log = ActionLog::new();

    loop {
        let player_before_action = game.player_to_act;

        clear_screen();

        if player_before_action == PlayerID::Player1 {
            println!("{}", game.render(false));
        }

        for log in &message_log {
            println!("{}", log);
        }

        println!("{}", action_log.render(false));

        if player_before_action == PlayerID::Player2 {
            let mut tree = mcts::Tree::new(game.clone());
            let recommended_action = tree.run();
            action_log.push((game.player_to_act, recommended_action.clone()));
            game = game.act(recommended_action);
            continue;
        }

        let mut input = String::new();
        io::stdin()
            .read_line(&mut input)
            .expect("Failed to read line");
        let input = input.to_lowercase();

        let command = Command::parse(&input);

        match command {
            Ok(Command::Action(action)) => {
                let action = if game.player_to_act == PlayerID::Player1 {
                    action
                } else {
                    action.reverse()
                };
                let available_actions = all_available_actions(&game);
                if available_actions.contains(&action) {
                    action_log.push((game.player_to_act, action.clone()));
                    game = game.act(action);
                    message_log.clear();
                } else {
                    message_log.push(String::from("Invalid action with state."));
                }
            }
            Ok(Command::Quit) => break,
            _ => message_log.push(String::from("Failed to parse input.")),
        }

        match game.terminal_state() {
            TerminalState::Draw => {
                clear_screen();
                println!("{}", game.render(false));
                println!("{}", action_log.render(false));
                println!("Game has ended in a draw.");
                break
            }
            TerminalState::Winner(player) => {
                clear_screen();
                println!("{}", game.render(false));
                println!("{}", action_log.render(false));
                println!("Player {} has won the game!", player.render(false));
                break
            }
            _ => (),
        }

        if player_before_action != game.player_to_act {
            clear_screen();

            println!("Switching players.\nPress Enter once switched.");

            let mut input = String::new();
            io::stdin()
                .read_line(&mut input)
                .expect("Failed to read line");
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use anvil::test_util::ReadableCoord;
    use anvil::test_util::ReadableCoord::*;

    impl Render for ReadableCoord {
        fn render(&self, active_player: bool) -> String {
            Coord::from(self.clone()).render(active_player)
        }
    }

    #[test]
    fn render_coord() {
        assert_eq!(MLR.render(false), "A1".to_string());
        assert_eq!(MRR.render(false), "A2".to_string());
        assert_eq!(MLD.render(false), "B1".to_string());
        assert_eq!(C.render(false), "D3".to_string());
        assert_eq!(TLLA.render(false), "E1".to_string());
        assert_eq!(TRR.render(false), "G2".to_string());
    }
}
