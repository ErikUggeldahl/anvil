use {AttackState, Card, CardIndex, Coord, Direction, FieldSpace, GameState, PlayerID};

use self::Action::*;
use CardType::*;
use FieldSpaceState::*;
use FieldSpaceType::*;
use PlayerID::*;

use std::mem;

#[derive(Clone, Debug, Hash, Eq, PartialEq)]
pub enum Action {
    Play(CardIndex, Vec<CardIndex>, Coord),
    Move(Coord, Direction),
    Attack(Coord, Direction, Option<CardIndex>),
    Defend(Option<CardIndex>, Vec<CardIndex>),
    End(Vec<CardIndex>),
}

impl GameState {
    pub fn act(self, action: Action) -> GameState {
        fn other_player(id: PlayerID) -> PlayerID {
            match id {
                Player1 => Player2,
                Player2 => Player1,
            }
        }

        fn play(
            mut state: GameState,
            play_index: CardIndex,
            discard_indices: &[CardIndex],
            to: Coord,
        ) -> GameState {
            let played = {
                let player = state.current_player_mut();

                let mut hand = player.hand.drain(..).map(Some).collect::<Vec<_>>();
                let played = mem::replace(hand.get_mut(play_index).unwrap(), None).unwrap();

                player.discard_pile.extend(
                    discard_indices
                        .iter()
                        .map(|&index| mem::replace(hand.get_mut(index).unwrap(), None).unwrap()),
                );
                player.hand = hand.into_iter().filter_map(|card| card).collect::<Vec<_>>();

                played
            };

            {
                let space = state.field.get_mut(&to).unwrap();
                space.state = match space.kind {
                    Advance => Ready(played),
                    _ => FaceDown(played),
                };
            }

            state
        }

        fn move_card(mut state: GameState, from: Coord, direction: Direction) -> GameState {
            let to = from.next(direction);

            let card = state.field.get_mut(&from).unwrap().get_card_mut();
            {
                state.field.get_mut(&to).unwrap().state = Moved(card);
            }

            state
        }

        fn attack(
            mut state: GameState,
            with: Coord,
            direction: Direction,
            augment: Option<CardIndex>,
        ) -> GameState {
            let defending_coord = with.next(direction);

            let evaporate = match state.field[&defending_coord].state {
                FaceDown(ref card)
                    if card.is_royal() && card.cost as usize > state.other_player().hand.len() =>
                {
                    true
                }
                _ => false,
            };

            if evaporate {
                let card = state
                    .field
                    .get_mut(&defending_coord)
                    .unwrap()
                    .get_card_mut();
                state.other_player_mut().discard_pile.push(card);
                return state;
            }

            // SO for better way of doing this
            if mem::discriminant(&state.field[&with].state)
                == mem::discriminant(&FaceDown(Card::new(Regular(0), Player1)))
            {
                let mut space = state.field.remove(&with).unwrap();
                if let FaceDown(card) = mem::replace(&mut space.state, Empty) {
                    mem::replace(&mut space.state, Ready(card));
                }
                state.field.insert(with, space);
            }

            let attack_augment = {
                let player = state.current_player_mut();
                augment.map(|index| player.hand.remove(index))
            };

            state.attack_state = Some(AttackState {
                attacker: with,
                defender: defending_coord,
                attack_augment,
            });
            state.player_to_act = other_player(state.player_to_act);

            state
        }

        fn defend(
            mut state: GameState,
            augment: Option<CardIndex>,
            discard: Vec<CardIndex>,
        ) -> GameState {
            // Possible to also accept the player here?
            // That way wouldn't need to rely on timing of swapping current player
            fn discard_loser(state: &mut GameState, coord: &Coord) {
                let card = state.field.get_mut(&coord).unwrap().get_card_mut();
                state.current_player_mut().discard_pile.push(card);
            };

            // Same as above
            fn discard_augment(state: &mut GameState, augment: Option<Card>) {
                if let Some(augment) = augment {
                    state.current_player_mut().discard_pile.push(augment);
                }
            }

            let defence_augment = {
                let player = state.current_player_mut();
                let mut hand = player.hand.drain(..).map(Some).collect::<Vec<_>>();
                let defence_augment =
                    augment.map(|index| mem::replace(hand.get_mut(index).unwrap(), None).unwrap());
                player.discard_pile.extend(
                    discard
                        .iter()
                        .map(|&index| mem::replace(hand.get_mut(index).unwrap(), None).unwrap()),
                );
                player.hand = hand.into_iter().filter_map(|card| card).collect::<Vec<_>>();
                defence_augment
            };

            let attack_state = state.attack_state.unwrap();
            state.attack_state = None;

            let attack_augment = attack_state.attack_augment;

            let attack_augment_value = attack_augment
                .as_ref()
                .map_or(0, |augment| augment.augment as u8);
            let defence_augment_value = defence_augment
                .as_ref()
                .map_or(0, |augment| augment.augment as u8);

            let defence_bonus = match state.field[&attack_state.defender] {
                FieldSpace {
                    owner: _,
                    state: FaceDown(_),
                    kind: Defence,
                } => 1,
                _ => 0,
            };

            let attack_value =
                state.field[&attack_state.attacker].get_card().rank + attack_augment_value;
            let defence_value = state.field[&attack_state.defender].get_card().rank
                + defence_augment_value + defence_bonus;

            if defence_value <= attack_value {
                discard_loser(&mut state, &attack_state.defender);
            } else {
                let space = state.field.get_mut(&attack_state.defender).unwrap();
                let card = { space.get_card_mut() };
                space.state = Ready(card);
            }

            discard_augment(&mut state, defence_augment);

            state.player_to_act = other_player(state.player_to_act);

            if attack_value <= defence_value {
                discard_loser(&mut state, &attack_state.attacker);
            } else {
                let space = state.field.get_mut(&attack_state.attacker).unwrap();
                let card = { space.get_card_mut() };
                space.state = Attacked(card);
            }

            discard_augment(&mut state, attack_augment);

            state
        }

        fn end(mut state: GameState, indices: &[CardIndex]) -> GameState {
            {
                let player = state.current_player_mut();

                let (discarded, new_hand) = player
                    .hand
                    .drain(..)
                    .enumerate()
                    .partition::<Vec<_>, _>(|&(index, _)| indices.contains(&index));

                player.hand = new_hand.into_iter().map(|(_, card)| card).collect();
                player
                    .discard_pile
                    .extend(discarded.into_iter().map(|(_, card)| card));
            }

            let next_player = other_player(state.players_turn);
            state.players_turn = next_player;
            state.player_to_act = next_player;

            {
                let player = state.current_player_mut();
                if let Some(card) = player.deck.pop() {
                    player.hand.push(card);
                }
            }

            let spaces_with_owned_cards = state
                .field
                .iter()
                .filter(|&(_, ref space)| match space.state {
                    Moved(ref card) | Attacked(ref card) if card.owner == next_player => true,
                    _ => false,
                })
                .map(|(coord, _)| *coord)
                .collect::<Vec<_>>();

            for &coord in &spaces_with_owned_cards {
                let space = state.field.get_mut(&coord).unwrap();
                let card = { space.get_card_mut() };
                space.state = Ready(card);
            }

            state
        }

        match action {
            Play(play_index, discard_indices, at) => play(self, play_index, &discard_indices, at),
            Move(from, direction) => move_card(self, from, direction),
            Attack(with, direction, augment) => attack(self, with, direction, augment),
            Defend(augment, discard) => defend(self, augment, discard),
            End(indices) => end(self, &indices),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use Direction::*;
    use test_util::ReadableCoord::*;

    #[test]
    fn play_removes_card_from_hand() {
        let mut game = GameState::new();
        game.add_to_hand_current(Regular(0));
        assert_eq!(game.current_player().hand.len(), 1);

        let play = Play(0, Vec::with_capacity(0), Coord::from(C));
        let new_state = game.act(play);
        assert_eq!(
            new_state.current_player().hand.len(),
            0,
            "Player did not have card removed from hand."
        );
    }

    #[test]
    fn play_discards_discard_cards() {
        let mut game = GameState::new();
        game.add_to_hand_current(Regular(0));
        game.add_to_hand_current(Regular(1));
        game.add_to_hand_current(Regular(2));
        let play = Play(0, vec![1, 2], Coord::from(C));
        let new_state = game.act(play);
        let player = new_state.current_player();
        assert_eq!(
            player.hand.len(),
            0,
            "Player did not have cards removed from hand."
        );
        assert_eq!(
            player.discard_pile.len(),
            2,
            "Player did not have cards moved to discard pile."
        );
    }

    #[test]
    fn play_adds_card_to_field() {
        let mut game = GameState::new();
        game.add_to_hand_current(Regular(0));
        game.add_to_hand_current(Regular(1));
        let play = Play(0, Vec::with_capacity(0), Coord::from(MRA));
        let new_state = game.act(play);
        let play = Play(0, Vec::with_capacity(0), Coord::from(MCD));
        let new_state = new_state.act(play);

        let attacking = &new_state.field[&Coord::from(MRA)];
        match &attacking.state {
            &Ready(ref card) => {
                assert_eq!(card.rank, 0, "Attacking card is not the first card played.")
            }
            _ => panic!("Attacking card is not Ready."),
        }
        let defending = &new_state.field[&Coord::from(MCD)];
        match &defending.state {
            &FaceDown(ref card) => assert_eq!(
                card.rank,
                1,
                "Defending card is not the second card played."
            ),
            _ => panic!("Defending card is not FaceDown."),
        }
    }

    #[test]
    fn move_empties_space() {
        let mut game = GameState::new();
        game.add_to_field_ready_current(C, Regular(0));
        let new_state = game.act(Move(Coord::from(C), East));

        let previous_field_space = new_state.field.get(&Coord::from(C)).unwrap();
        match previous_field_space.state {
            Empty => (),
            _ => panic!("Previous field space was not emptied."),
        }
    }

    #[test]
    fn move_moves_card() {
        let mut game = GameState::new();
        game.add_to_field_ready_current(C, Regular(0));
        let new_state = game.act(Move(Coord::from(C), East));

        let next_field_space = new_state.field.get(&Coord::from(C).next(East));
        assert!(next_field_space.is_some());
        let next_field_space = next_field_space.unwrap();
        let ref state = next_field_space.state;
        let card = match state {
            &Moved(ref card) => card,
            _ => panic!("New space is not in state Moved."),
        };
        match card.rank {
            0 => (),
            _ => panic!("Moved card is not the same type."),
        }
    }

    #[test]
    fn attack_removes_augment_from_hand() {
        let mut game = GameState::new();
        game.add_to_hand_current(Regular(0));

        let new_state = game.act(Attack(Coord::from(C), East, Some(0)));
        let player = new_state.current_player();
        assert_eq!(
            player.hand.len(),
            0,
            "Augment was not removed from player's hand."
        );
    }

    #[test]
    fn attack_creates_attack_state() {
        let mut game = GameState::new();
        game.add_to_hand_current(Regular(0));

        let new_state = game.act(Attack(Coord::from(RC), East, Some(0)));
        assert!(
            new_state.attack_state.is_some(),
            "Attack state was not created."
        );
        let attack_state = new_state.attack_state.unwrap();
        assert_eq!(
            attack_state.attack_augment.unwrap().rank,
            0,
            "Augment was not moved to attack state."
        );
        assert_eq!(
            attack_state.attacker,
            Coord::from(RC),
            "Attacking coordinate not set properly."
        );
        assert_eq!(
            attack_state.defender,
            Coord::from(RC).next(East),
            "Defending coordinate not set properly."
        );
    }

    #[test]
    fn attack_switches_player_to_act() {
        let mut game = GameState::new();
        game.add_to_hand_current(Regular(0));

        let new_state = game.clone().act(Attack(Coord::from(RC), East, Some(0)));
        assert!(
            new_state.player_to_act != game.player_to_act,
            "End did not switch player to act."
        );
    }

    #[test]
    fn attack_evaporates_unpaid_royals() {
        let mut game = GameState::new();

        game.add_to_field(MLD, Ready(Card::new(Regular(0), Player1)));
        game.add_to_field(MLR, FaceDown(Card::new(LargeRoyal, Player2)));

        game.add_to_hand_current(Regular(0));

        let new_state = game.act(Attack(Coord::from(MLD), SouthEast, Some(0)));

        assert_eq!(
            new_state.field[&Coord::from(MLR)].state,
            Empty,
            "Defending royal was not evaporated."
        );
        assert_eq!(
            new_state.current_player().hand.len(),
            1,
            "Augment was incorrectly removed from attacking player's hand."
        );
        assert_eq!(
            new_state.field[&Coord::from(MLD)].state,
            Ready(Card::new(Regular(0), Player1)),
            "Attacking card incorrectly changed state."
        );
    }

    #[test]
    fn attack_flips_face_down_attacker() {
        let mut game = GameState::new();
        game.add_to_field(MCD, FaceDown(Card::new(Regular(0), Player1)));
        game.add_to_field(MLA, Ready(Card::new(Regular(0), Player2)));

        let new_state = game.act(Attack(Coord::from(MCD), NorthWest, None));

        match new_state.field[&Coord::from(MCD)].state {
            Ready(_) => (),
            _ => panic!("Face down attacker should be flipped to ready."),
        };

        let new_state = new_state.act(Defend(None, Vec::new()));

        match new_state.field[&Coord::from(MCD)].state {
            Empty => (),
            _ => panic!("Attacker should be destroyed."),
        };
        match new_state.field[&Coord::from(MCD)].state {
            Empty => (),
            _ => panic!("Defender should be destroyed."),
        };
    }

    fn create_defend_test_setup() -> GameState {
        let mut game = GameState::new();
        // Defender
        game.add_to_hand_current(SmallAugment);
        game.add_to_field_ready_current(C, Regular(1));

        // Attacker
        game.players_turn = Player2;
        game.player_to_act = Player2;
        game.add_to_hand_current(SmallAugment);
        game.add_to_field_ready_current(LC, Regular(1));

        game
    }

    #[test]
    fn defend_removes_augment_from_hand() {
        let game = create_defend_test_setup();
        let new_state = game.act(Attack(Coord::from(LC), West, None));
        let new_state = new_state.act(Defend(Some(0), Vec::new()));
        assert_eq!(
            new_state.other_player().hand.len(),
            0,
            "Augment was not removed from hand."
        );
    }

    #[test]
    fn defend_cleans_attack_state() {
        let game = create_defend_test_setup();
        let new_state = game.act(Attack(Coord::from(LC), West, None));
        let new_state = new_state.act(Defend(Some(0), Vec::new()));
        assert!(
            new_state.attack_state.is_none(),
            "Attack state not cleared after defense."
        );
    }

    #[test]
    fn defend_discards_losers() {
        let game = create_defend_test_setup();
        let both_lose_state = game.clone().act(Attack(Coord::from(LC), West, None));
        let both_lose_state = both_lose_state.act(Defend(None, Vec::new()));

        match both_lose_state.field[&Coord::from(C)].state {
            Empty => (),
            _ => panic!("Defender was not removed from field."),
        }

        match both_lose_state.field[&Coord::from(LC)].state {
            Empty => (),
            _ => panic!("Attacker was not removed from field."),
        }

        let attacker_loses_state = game.clone().act(Attack(Coord::from(LC), West, None));
        let attacker_loses_state = attacker_loses_state.act(Defend(Some(0), Vec::new()));

        match attacker_loses_state.field[&Coord::from(C)].state {
            Ready(_) => (),
            _ => panic!("Defender was incorrectly removed from field."),
        }

        match attacker_loses_state.field[&Coord::from(LC)].state {
            Empty => (),
            _ => panic!("Attacker was not removed from field."),
        }

        let defender_loses_state = game.clone().act(Attack(Coord::from(LC), West, Some(0)));
        let defender_loses_state = defender_loses_state.act(Defend(None, Vec::new()));

        match defender_loses_state.field[&Coord::from(C)].state {
            Empty => (),
            _ => panic!("Defender was not removed from field."),
        }

        match defender_loses_state.field[&Coord::from(LC)].state {
            Attacked(_) => (),
            _ => panic!("Attacker was incorrectly removed from field."),
        }
    }

    #[test]
    fn defend_adds_defence_bonus() {
        let mut game = GameState::new();

        game.add_to_field(MCD, FaceDown(Card::new(Regular(1), Player1)));
        game.add_to_field(MLR, FaceDown(Card::new(SmallRoyal, Player1)));
        game.add_to_field(MLA, Ready(Card::new(Regular(1), Player2)));
        game.add_to_field(MRA, Ready(Card::new(Regular(1), Player2)));
        game.add_to_field(MLD, Ready(Card::new(SmallRoyal, Player2)));

        game.add_to_hand_current(Regular(0));

        game.players_turn = Player2;
        game.player_to_act = Player2;

        let new_state = game.act(Attack(Coord::from(MLA), SouthEast, None));
        let new_state = new_state.act(Defend(None, Vec::new()));

        match new_state.field[&Coord::from(MCD)].state {
            Ready(_) => (),
            _ => panic!("Defender was incorrectly removed from field."),
        }
        assert_eq!(
            new_state.field[&Coord::from(MLA)].state,
            Empty,
            "Attacker was not removed from field."
        );

        let new_state = new_state.act(Attack(Coord::from(MRA), SouthWest, None));
        let new_state = new_state.act(Defend(None, Vec::new()));

        assert_eq!(
            new_state.field[&Coord::from(MRA)].state,
            Empty,
            "Attacker was not removed from field."
        );
        assert_eq!(
            new_state.field[&Coord::from(MCD)].state,
            Empty,
            "Defender was not removed from field."
        );

        let new_state = new_state.act(Attack(Coord::from(MLD), SouthEast, None));
        let new_state = new_state.act(Defend(None, vec![0]));

        assert_eq!(
            new_state.field[&Coord::from(MLD)].state,
            Empty,
            "Attacker was not removed from field."
        );
        assert_eq!(
            new_state.field[&Coord::from(MLR)].state,
            Empty,
            "Defender was not removed from field."
        );
    }

    #[test]
    fn defend_discards_augments() {
        let game = create_defend_test_setup();
        let new_state = game.act(Attack(Coord::from(LC), West, Some(0)));
        let new_state = new_state.act(Defend(Some(0), Vec::new()));

        assert_eq!(
            new_state.player1.discard_pile.len(),
            2,
            "Player does not have the expected number of cards discarded."
        );
        assert_eq!(
            new_state.player2.discard_pile.len(),
            2,
            "Player does not have the expected number of cards discarded."
        );

        assert!(
            new_state
                .player1
                .discard_pile
                .contains(&Card::new(SmallAugment, Player1,),),
            "Player's discard pile does not contain the used augment."
        );

        assert!(
            new_state
                .player2
                .discard_pile
                .contains(&Card::new(SmallAugment, Player2,),),
            "Player's discard pile does not contain the used augment."
        );
    }

    #[test]
    fn defend_discards_discard_cards() {
        let mut game = GameState::new();

        game.add_to_field(MLR, FaceDown(Card::new(LargeRoyal, Player1)));
        game.add_to_field(MLD, Ready(Card::new(SmallRoyal, Player2)));

        game.add_to_hand_current(Regular(0));
        game.add_to_hand_current(Regular(1));
        game.add_to_hand_current(Regular(2));

        game.players_turn = Player2;
        game.player_to_act = Player2;

        let new_state = game.act(Attack(Coord::from(MLD), SouthEast, None));
        let new_state = new_state.act(Defend(None, vec![0, 2]));

        assert_eq!(
            new_state.player1.hand.len(),
            1,
            "Discard cards were not removed from hand."
        );
        assert_eq!(
            new_state.player1.hand[0],
            Card::new(Regular(1), Player1),
            "Discarded cards are not those indicated by indices."
        );
    }

    #[test]
    fn defend_reveals_face_down_cards() {
        let mut game = GameState::new();

        game.add_to_field(MCD, FaceDown(Card::new(Regular(1), Player1)));
        game.add_to_field(MLR, FaceDown(Card::new(SmallRoyal, Player1)));
        game.add_to_field(MLA, Ready(Card::new(Regular(0), Player2)));
        game.add_to_field(MLD, Ready(Card::new(Regular(0), Player2)));

        game.add_to_hand_current(Regular(0));

        game.players_turn = Player2;
        game.player_to_act = Player2;

        let new_state = game.act(Attack(Coord::from(MLA), SouthEast, None));
        let new_state = new_state.act(Defend(None, Vec::new()));

        match new_state.field[&Coord::from(MCD)].state {
            Ready(_) => (),
            _ => panic!("Defender was not revealed."),
        }

        let new_state = new_state.act(Attack(Coord::from(MLD), SouthEast, None));
        let new_state = new_state.act(Defend(None, vec![0]));

        match new_state.field[&Coord::from(MLR)].state {
            Ready(_) => (),
            _ => panic!("Defender was not revealed."),
        }
    }

    #[test]
    fn defend_switches_player_to_act() {
        let game = create_defend_test_setup();
        let new_state = game.clone().act(Attack(Coord::from(LC), West, None));
        let new_state = new_state.act(Defend(None, Vec::new()));

        assert!(new_state.player_to_act == game.player_to_act);
    }

    #[test]
    fn end_changes_players() {
        let game = GameState::new();
        let new_state = game.clone().act(End(Vec::new()));

        assert!(
            game.players_turn != new_state.players_turn,
            "End did not switch players' turn."
        );
        assert!(
            game.player_to_act != new_state.player_to_act,
            "End did not switch player to act."
        );
    }

    #[test]
    fn end_discards_overage_cards() {
        let mut game = GameState::new();

        for _ in 0..9 {
            game.add_to_hand_current(Regular(0));
        }

        let new_state = game.act(End(vec![0, 1]));

        assert_eq!(
            new_state.other_player().hand.len(),
            7,
            "End did not discard overage cards."
        );
    }

    #[test]
    fn end_draws_card_to_hand() {
        let mut game = GameState::new();
        let old_hand_length = game.other_player().hand.len();

        game.other_player_mut()
            .deck
            .push(Card::new(Regular(0), Player2));

        let new_state = game.act(End(Vec::new()));
        let new_hand_length = new_state.current_player().hand.len();
        assert_eq!(
            old_hand_length + 1,
            new_hand_length,
            "Card was not added to hand."
        );

        let new_card_rank = new_state.current_player().hand.last().unwrap().rank;
        assert_eq!(new_card_rank, 0, "Card added was not the last in the deck.");
    }

    #[test]
    fn end_draws_card_from_deck() {
        let game = GameState::new();
        let old_deck_count = game.other_player().deck.len();
        let new_state = game.act(End(Vec::new()));
        let new_deck_count = new_state.current_player().deck.len();
        assert_eq!(
            old_deck_count - 1,
            new_deck_count,
            "Card was not removed from deck."
        );
    }

    #[test]
    fn end_draw_empty_does_not_crash() {
        let mut game = GameState::new();
        for _ in 0..54 {
            game = game.act(End(Vec::new()));
        }

        assert_eq!(game.current_player().deck.len(), 0);
        assert_eq!(game.other_player().deck.len(), 0);
    }

    #[test]
    fn end_readies_unready_cards() {
        let mut game = create_defend_test_setup();

        game.add_to_field_ready_current(RC, Regular(0));

        let new_state = game.act(Attack(Coord::from(LC), West, Some(0)));
        let new_state = new_state.act(Defend(None, Vec::new()));
        let new_state = new_state.act(Move(Coord::from(RC), SouthEast));

        assert!(
            new_state
                .field
                .iter()
                .all(|(_, ref space)| match space.state {
                    Ready(_) => false,
                    _ => true,
                },),
            "There should be no spaces in a ready state after movement and combat."
        );

        let new_state = new_state.act(End(Vec::new()));
        let new_state = new_state.act(End(Vec::new()));

        assert!(
            new_state
                .field
                .iter()
                .all(|(_, ref space)| match space.state {
                    Moved(_) | Attacked(_) => false,
                    _ => true,
                },),
            "There should be no spaces in a moved or attacked state after the turn begins."
        );
    }
}
