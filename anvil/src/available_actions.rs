use {AttackState, Augment, Card, CardIndex, Coord, Direction, Field, FieldSpace, FieldSpaceType, full_royals, GameState, Hand, PlayerID};
use action::Action;

use action::Action::*;
use Direction::*;
use FieldSpaceState::*;
use FieldSpaceType::*;
use PlayerID::*;

fn valid_turn(player_turn: PlayerID, player_to_act: PlayerID) -> bool {
    player_turn == player_to_act
}

fn valid_destination_state(space: &FieldSpace) -> bool {
    match space.state {
        Empty => true,
        _ => false,
    }
}

fn valid_card(space: &FieldSpace, player: PlayerID) -> bool {
    match space.state {
        Ready(ref card) if card.owner == player => true,
        _ => false,
    }
}

fn valid_direction(direction: Direction, player: PlayerID) -> bool {
    match player {
        Player1 => match direction {
            NorthWest | NorthEast => true,
            _ => false,
        },
        Player2 => match direction {
            SouthWest | SouthEast => true,
            _ => false,
        },
    }
}

fn valid_coord(coord: &Coord) -> bool {
    match *coord {
        Coord(x, y, z) if x.abs() <= 2 && y.abs() <= 2 && z.abs() <= 3 && x + y + z == 0 => true,
        _ => false,
    }
}

fn valid_augment(index: CardIndex, hand: &Hand) -> bool {
    match hand[index].augment {
        Augment::NoAugment => false,
        _ => true,
    }
}

fn hand_combinations(hand_size: u8) -> Vec<Vec<CardIndex>> {
    // Hamming weight is the population of a binary number, i.e. number of 1's.
    // We can use this to filter combinations above a certain size.
    fn hamming_weight(mut n: u16) -> u8 {
        let mut count = 0u8;
        while n > 0 {
            count += (n & 1) as u8;
            n >>= 1;
        }
        count
    }

    let mut combinations: Vec<Vec<CardIndex>> = Vec::new();
    for mut i in 0..(2 as u16).pow(hand_size as u32) {
        let hamming_weight = hamming_weight(i);
        if hamming_weight > 2 {
            continue;
        }
        let mut current: Vec<CardIndex> = Vec::with_capacity(hamming_weight as usize);
        let mut index = 0;
        while i != 0 {
            if i & 1 == 1 {
                current.push(index);
            }
            index += 1;
            i >>= 1;
        }
        combinations.push(current);
    }
    combinations
}

pub fn available_plays(state: &GameState) -> Vec<Action> {
    if !valid_turn(state.players_turn, state.player_to_act) {
        return Vec::<_>::new();
    }

    fn valid_destination_owner(space: &FieldSpace, player: PlayerID) -> bool {
        match space.owner {
            Some(owner) if owner == player => true,
            _ => false,
        }
    }

    fn valid_discard_length_for_cost(
        card: &Card,
        discard_indices: &Vec<CardIndex>,
        destination_type: FieldSpaceType,
    ) -> bool {
        match destination_type {
            Advance => card.cost as u8 == discard_indices.len() as u8,
            _ => discard_indices.len() == 0,
        }
    }

    fn valid_discard_set(play_index: CardIndex, discard_indices: &Vec<CardIndex>) -> bool {
        !discard_indices.contains(&play_index)
    }

    fn valid_destination_for_type(
        card: &Card,
        field: &Field,
        player: PlayerID,
        coord: &Coord,
    ) -> bool {
        // Royal spaces must be full for royals to be played elsewhere
        if card.is_royal() && !field[&coord].kind.is_royal() && !full_royals(field, player) ||
        // Non-royal cards cannot be played in royal spaces
            !card.is_royal() && field[&coord].kind.is_royal()
        {
            false
        } else {
            true
        }
    }

    let play_indices = 0..state.current_player().hand.len();
    let discard_indices = hand_combinations(state.current_player().hand.len() as u8);

    let coords = state
        .field
        .iter()
        .filter(|&(coord, _)| {
            let space = &state.field[coord];
            valid_destination_state(space) && valid_destination_owner(space, state.player_to_act)
        })
        .map(|(coord, _)| coord)
        .collect::<Vec<_>>();

    let mut plays = Vec::<Action>::new();
    for play_index in play_indices {
        for discard_indices in &discard_indices {
            // Validate based on the play and discard indices
            let card = &state.current_player().hand[play_index];
            if !valid_discard_set(play_index, discard_indices) {
                continue;
            }

            for &&coord in &coords {
                // Validate based on coordinate
                if !valid_destination_for_type(card, &state.field, state.player_to_act, &coord)
                    || !valid_discard_length_for_cost(
                        card,
                        discard_indices,
                        state.field[&coord].kind,
                    ) {
                    continue;
                }

                plays.push(Play(play_index, discard_indices.clone(), coord));
            }
        }
    }

    plays
}

pub fn available_moves(state: &GameState) -> Vec<Action> {
    if !valid_turn(state.players_turn, state.player_to_act) {
        return Vec::<_>::new();
    }

    let player = state.player_to_act;

    fn valid_origin_kind(space: &FieldSpace, player: PlayerID) -> bool {
        match space.owner {
            Some(owner) if owner == player => match space.kind {
                Advance => true,
                _ => false,
            },
            _ => true,
        }
    }

    let coords = state
        .field
        .iter()
        .filter(|&(_, space)| valid_card(space, player) && valid_origin_kind(space, player))
        .map(|(coord, _)| coord);

    let directions = [NorthWest, NorthEast, SouthWest, SouthEast];

    let directions = directions
        .into_iter()
        .filter(|&&direction| valid_direction(direction, player))
        .collect::<Vec<_>>();

    let mut moves = Vec::<Action>::new();

    for &coord in coords {
        for &&direction in &directions {
            let next_coord = coord.next(direction);
            if valid_coord(&next_coord) && valid_destination_state(&state.field[&next_coord]) {
                moves.push(Move(coord, direction));
            }
        }
    }

    moves
}

pub fn available_attacks(state: &GameState) -> Vec<Action> {
    if !valid_turn(state.players_turn, state.player_to_act) {
        return Vec::<_>::new();
    }

    let player = state.player_to_act;

    fn valid_card(space: &FieldSpace, player: PlayerID) -> bool {
        match space.state {
            Ready(ref card) | Moved(ref card) | FaceDown(ref card) if card.owner == player => true,
            _ => false,
        }
    }

    fn valid_origin_kind(space: &FieldSpace, player: PlayerID) -> bool {
        match space.owner {
            Some(owner) if owner == player => match space.kind {
                Advance | Defence => true,
                _ => false,
            },
            _ => true,
        }
    }

    fn valid_destination_state(space: &FieldSpace, attacker: PlayerID) -> bool {
        match space.state {
            Ready(ref card) | Moved(ref card) | Attacked(ref card) | FaceDown(ref card)
                if card.owner != attacker =>
            {
                true
            }
            _ => false,
        }
    }

    let coords = state
        .field
        .iter()
        .filter(|&(_, space)| valid_card(space, player) && valid_origin_kind(space, player))
        .map(|(coord, _)| coord);

    let directions = [NorthWest, NorthEast, SouthWest, SouthEast];

    let directions = directions
        .into_iter()
        .filter(|&&direction| valid_direction(direction, player))
        .collect::<Vec<_>>();

    let mut augment_indices = (0..state.current_player().hand.len())
        .filter(|&index| valid_augment(index, &state.current_player().hand))
        .map(Some)
        .collect::<Vec<_>>();
    augment_indices.push(None);

    let mut attacks = Vec::<Action>::new();

    for &coord in coords {
        for &&direction in &directions {
            let next_coord = coord.next(direction);
            if !valid_coord(&next_coord)
                || !valid_destination_state(&state.field[&next_coord], player)
            {
                continue;
            }
            for &augment_index in &augment_indices {
                attacks.push(Attack(coord, direction, augment_index))
            }
        }
    }

    attacks
}

pub fn available_defences(state: &GameState) -> Vec<Action> {
    fn valid_attack_state(attack_state: &Option<AttackState>) -> bool {
        attack_state.is_some()
    }

    fn valid_discard_set(
        augment_index: &Option<CardIndex>,
        discard_indices: &Vec<CardIndex>,
    ) -> bool {
        match &augment_index {
            &&Some(augment_index) => !discard_indices.contains(&augment_index),
            &&None => true,
        }
    }

    fn valid_discard_length_for_cost(
        card: &Card,
        discard_indices: &Vec<CardIndex>,
        hand_length: usize,
        augment: &Option<CardIndex>,
    ) -> bool {
        if hand_length < card.cost as usize {
            discard_indices.len() == 0 && augment.is_none()
        } else {
            discard_indices.len() == card.cost as usize
        }
    }

    if !valid_attack_state(&state.attack_state) {
        return Vec::<_>::new();
    }

    let mut augment_indices = (0..state.current_player().hand.len())
        .filter(|&index| valid_augment(index, &state.current_player().hand))
        .map(Some)
        .collect::<Vec<_>>();
    augment_indices.push(None);

    let discard_indices = hand_combinations(state.current_player().hand.len() as u8);

    let defence_coord = state.attack_state.as_ref().unwrap().defender;
    let defence_card = state.field[&defence_coord].get_card();

    let mut defences = Vec::<Action>::new();
    for augment_index in augment_indices {
        for discard_indices in &discard_indices {
            if !valid_discard_set(&augment_index, discard_indices)
                || !valid_discard_length_for_cost(
                    defence_card,
                    discard_indices,
                    state.current_player().hand.len(),
                    &augment_index,
                ) {
                continue;
            }

            defences.push(Defend(augment_index, discard_indices.clone()));
        }
    }

    defences
}

pub fn available_ends(state: &GameState) -> Vec<Action> {
    fn valid_discard_length_for_overage(overage: u8, discard_indices: &Vec<CardIndex>) -> bool {
        overage == discard_indices.len() as u8
    }

    let player = &state.current_player();

    if !valid_turn(state.players_turn, state.player_to_act) {
        Vec::new()
    } else if player.hand.len() <= 7 {
        vec![End(Vec::new())]
    } else {
        let discard_indices = hand_combinations(player.hand.len() as u8);
        let mut ends = Vec::new();

        for discard_indices in &discard_indices {
            if !valid_discard_length_for_overage(player.hand.len() as u8 - 7, discard_indices) {
                continue;
            }

            ends.push(End(discard_indices.clone()));
        }

        ends
    }
}

pub fn all_available_actions(state: &GameState) -> Vec<Action> {
    let mut all = available_plays(state);
    all.append(&mut available_moves(state));
    all.append(&mut available_attacks(state));
    all.append(&mut available_defences(state));
    all.append(&mut available_ends(state));
    all
}

#[cfg(test)]
mod tests {
    use super::*;
    use CardType::*;
    use test_util::ReadableCoord::*;

    #[test]
    fn no_available_plays_in_empty_hand() {
        let game = GameState::new();

        let plays = available_plays(&game);
        assert_eq!(
            plays.len(),
            0,
            "There should be no available plays in an empty game."
        );
    }

    #[test]
    fn no_available_plays_while_not_turn() {
        let mut game = GameState::new();

        game.add_to_field_ready_current(C, Regular(0));
        game.add_to_field_ready(TRA, Regular(0), Player2);

        let new_state = game.act(Attack(Coord::from(C), NorthEast, None));

        let plays = available_plays(&new_state);
        assert_eq!(plays.len(), 0, "There should be no plays while defending.");
    }

    #[test]
    fn available_plays_filters_by_player() {
        let mut game = GameState::new();
        game.add_to_hand(Regular(0), Player2);

        let plays = available_plays(&game);
        assert_eq!(
            plays.len(),
            0,
            "There should be no available plays using opponent's hand."
        );
    }

    #[test]
    fn available_plays_filters_coords_for_regular_card_type() {
        let mut game = GameState::new();
        game.add_to_hand_current(Regular(0));

        let plays = available_plays(&game);
        assert_eq!(
            plays.len(),
            7,
            "A regular card should have seven available plays in an empty field."
        );

        game.add_to_field_ready_current(MCD, Regular(0));

        let plays = available_plays(&game);
        assert_eq!(
            plays.len(),
            6,
            "A regular card should have six available plays in a field with one space occupied."
        );
    }

    #[test]
    fn available_plays_filters_coords_for_royal_card_type() {
        let mut game = GameState::new();
        game.add_to_hand_current(SmallRoyal);
        game.add_to_hand_current(SmallRoyal);
        game.add_to_hand_current(SmallRoyal);

        let plays = available_plays(&game);
        assert_eq!(
            plays.len(),
            2 * 3,
            "Three small royals should each have two available plays in an empty field."
        );

        game.add_to_field_ready_current(MLR, SmallRoyal);

        let plays = available_plays(&game);
        assert_eq!(
            plays.len(),
            1 * 3,
            "Three small royals should each have one available play \
             in a field with one royal space occupied."
        );

        game.add_to_field_ready_current(MRR, SmallRoyal);

        let plays = available_plays(&game);
        assert_eq!(
            plays.len(),
            (4 * 3 * 2) + // 4 attack spaces, 3 royals, 2 possible discards
            (3 * 3), // 3 defence spaces, 3 royals
            "Three small royals should each have eleven available plays \
             in a field with both royal spaces occupied."
        );
    }

    #[test]
    fn available_plays_combines_hand_discards() {
        let mut game = GameState::new();
        game.add_to_hand_current(Regular(0));
        game.add_to_hand_current(Regular(0));
        game.add_to_hand_current(Regular(0));
        game.add_to_hand_current(Regular(0));
        game.add_to_hand_current(SmallRoyal);
        game.add_to_hand_current(LargeRoyal);

        let plays = available_plays(&game);
        let regular_plays = 7 * 4; // 7 spaces, 4 regular cards
        let small_royal_plays = 2; // 2 royal spaces
        let large_royal_plays = 2; // 2 royal spaces
        assert_eq!(
            plays.len(),
            regular_plays + small_royal_plays + large_royal_plays,
            "Sum of possible plays with combinations did not total as expected."
        );
    }

    #[test]
    fn no_available_moves_in_empty_field() {
        let game = GameState::new();

        let moves = available_moves(&game);
        assert_eq!(
            moves.len(),
            0,
            "There should be no available moves in an empty game."
        );
    }

    #[test]
    fn no_available_moves_while_not_turn() {
        let mut game = GameState::new();

        game.add_to_field_ready_current(C, Regular(0));
        game.add_to_field_ready(TRA, Regular(0), Player2);

        let new_state = game.act(Attack(Coord::from(C), NorthEast, None));

        let moves = available_moves(&new_state);
        assert_eq!(moves.len(), 0, "There should be no moves while defending.");
    }

    #[test]
    fn available_moves_filters_by_player() {
        let mut game = GameState::new();
        game.add_to_field_ready(C, Regular(1), Player2);

        let moves = available_moves(&game);
        assert_eq!(
            moves.len(),
            0,
            "Should not be able to move opponent's card."
        );
    }

    #[test]
    fn available_moves_filters_by_direction() {
        let mut game = GameState::new();
        game.add_to_field_ready(C, Regular(1), Player1);
        game.add_to_field_ready(LC, Regular(1), Player2);

        let moves = available_moves(&game);
        assert_eq!(
            moves.len(),
            2,
            "Card should have exactly two move actions available."
        );
        assert!(
            moves.contains(&Move(Coord::from(C), NorthEast)),
            "Player 1 card should be able to move north east."
        );
        assert!(
            moves.contains(&Move(Coord::from(C), NorthWest)),
            "Player 1 card should be able to move north west."
        );

        let new_state = game.act(End(Vec::new()));
        let moves = available_moves(&new_state);
        assert_eq!(
            moves.len(),
            2,
            "Card should have exactly two move actions available."
        );
        assert!(
            moves.contains(&Move(Coord::from(LC), SouthEast)),
            "Player 2 card should be able to move south east."
        );
        assert!(
            moves.contains(&Move(Coord::from(LC), SouthWest)),
            "Player 2 card should be able to move south west."
        );
    }

    #[test]
    fn available_moves_filters_by_valid_coord() {
        let mut game = GameState::new();
        game.add_to_field_ready_current(LLC, Regular(1));
        game.add_to_field_ready_current(TRR, Regular(1));

        let moves = available_moves(&game);
        assert_eq!(
            moves.len(),
            1,
            "Card should have exactly one move action available."
        );
        assert!(
            moves.contains(&Move(Coord::from(LLC), NorthWest)),
            "Player 1 card should be able to move north west."
        );
    }

    #[test]
    fn available_moves_filters_occupied() {
        let mut game = GameState::new();
        game.add_to_field_ready_current(C, Regular(1));
        game.add_to_field_ready(TRA, Regular(1), Player2);

        let moves = available_moves(&game);
        assert_eq!(
            moves.len(),
            1,
            "Card should have exactly one move action available."
        );
        assert!(
            moves.contains(&Move(Coord::from(C), NorthWest)),
            "Player 1 card should be able to move north west."
        );
    }

    #[test]
    fn available_moves_filters_unready_cards() {
        let mut game = GameState::new();
        game.add_to_field(C, Moved(Card::new(Regular(1), Player1)));
        game.add_to_field(LC, Attacked(Card::new(Regular(1), Player1)));
        game.add_to_field(LLC, FaceDown(Card::new(Regular(1), Player1)));

        let moves = available_moves(&game);
        assert_eq!(
            moves.len(),
            0,
            "Card should have exactly zero move actions available."
        );
    }

    #[test]
    fn available_moves_filters_defence_cards() {
        let mut game = GameState::new();
        game.add_to_field_ready_current(MCD, Regular(1));
        game.add_to_field_ready_current(MLR, Regular(1));

        let moves = available_moves(&game);
        assert_eq!(
            moves.len(),
            0,
            "Card should have exactly zero move actions available."
        );
    }

    #[test]
    fn no_available_attacks_in_empty_field() {
        let game = GameState::new();

        let attacks = available_attacks(&game);
        assert_eq!(
            attacks.len(),
            0,
            "There should be no available attacks in an empty game."
        );
    }

    #[test]
    fn no_available_attacks_while_not_turn() {
        let mut game = GameState::new();

        game.add_to_field_ready_current(C, Regular(0));
        game.add_to_field_ready(TRA, Regular(0), Player2);

        let new_state = game.act(Attack(Coord::from(C), NorthEast, None));

        let attacks = available_attacks(&new_state);
        assert_eq!(
            attacks.len(),
            0,
            "There should be no attacks while defending."
        );
    }

    #[test]
    fn available_attacks_filters_by_player() {
        let mut game = GameState::new();

        game.add_to_field_ready(C, Regular(0), Player2);
        game.add_to_field_ready(TRA, Regular(0), Player2);

        let attacks = available_attacks(&game);
        assert_eq!(
            attacks.len(),
            0,
            "There should be no available attacks using opponent's cards."
        );
    }

    #[test]
    fn available_attacks_filters_unready_cards() {
        use std::collections::HashSet;

        let mut game = GameState::new();

        game.add_to_field(MCD, Ready(Card::new(Regular(0), Player1)));
        game.add_to_field(MRD, FaceDown(Card::new(Regular(0), Player1)));
        game.add_to_field(MLD, Attacked(Card::new(Regular(0), Player1)));

        game.add_to_field_ready(MLLA, Regular(0), Player2);
        game.add_to_field_ready(MLA, Regular(0), Player2);
        game.add_to_field_ready(MRA, Regular(0), Player2);
        game.add_to_field_ready(MRRA, Regular(0), Player2);

        let attacks = available_attacks(&game)
            .into_iter()
            .collect::<HashSet<Action>>();
        assert_eq!(
            attacks,
            [
                Attack(Coord::from(MCD), NorthEast, None),
                Attack(Coord::from(MCD), NorthWest, None),
                Attack(Coord::from(MRD), NorthWest, None),
                Attack(Coord::from(MRD), NorthEast, None),
            ].iter().cloned().collect::<HashSet<Action>>(),
            "Only the ready card and face down card should have each two attacks actions available."
        );
    }

    #[test]
    fn available_attacks_filters_royal_row_cards() {
        let mut game = GameState::new();

        game.add_to_field_ready_current(MRR, SmallRoyal);
        game.add_to_field_ready(MCD, Regular(0), Player2);

        let attacks = available_attacks(&game);
        assert_eq!(
            attacks.len(),
            0,
            "Cards in the royal row should not be able to attack."
        );
    }

    #[test]
    fn available_attacks_filters_direction() {
        let mut game = GameState::new();

        game.add_to_field_ready_current(C, Regular(0));
        game.add_to_field_ready(MLA, Regular(0), Player2);
        game.add_to_field_ready(MRA, Regular(0), Player2);
        game.add_to_field_ready(TRA, Regular(0), Player2);
        game.add_to_field_ready(TLA, Regular(0), Player2);
        game.add_to_field_ready(LC, Regular(0), Player2);
        game.add_to_field_ready(RC, Regular(0), Player2);

        let attacks = available_attacks(&game);
        assert_eq!(
            attacks.len(),
            2,
            "Cards should only be able to attack in the forwards direction."
        );
    }

    #[test]
    fn available_attacks_filters_by_valid_coord() {
        let mut game = GameState::new();

        game.add_to_field_ready_current(RRC, Regular(0));
        game.add_to_field_ready(TLLA, Regular(0), Player2);

        let attacks = available_attacks(&game);
        assert_eq!(
            attacks.len(),
            1,
            "Cards should only be able to attack to a valid coordinate destination."
        );
    }

    #[test]
    fn available_attacks_filters_by_defending_owner() {
        let mut game = GameState::new();

        game.add_to_field_ready_current(C, Regular(0));
        game.add_to_field_ready_current(TRA, Regular(0));

        let attacks = available_attacks(&game);
        assert_eq!(
            attacks.len(),
            0,
            "Cards should not be able to attack cards of the same owner."
        );
    }

    #[test]
    fn available_attacks_provides_augments() {
        let mut game = GameState::new();

        game.add_to_field_ready_current(C, Regular(0));
        game.add_to_field_ready(TRA, Regular(0), Player2);
        game.add_to_field_ready(TLA, Regular(0), Player2);

        game.add_to_hand_current(Regular(0));
        game.add_to_hand_current(SmallAugment);
        game.add_to_hand_current(SmallAugment);
        game.add_to_hand_current(LargeAugment);

        let attacks = available_attacks(&game);
        assert_eq!(
            attacks.len(),
            // Two opponents with three augments in hand plus one option to not play an augment.
            2 * 4,
            "Cards should only be able to use augment cards."
        );

        assert!(
            attacks.contains(&Attack(Coord::from(C), NorthEast, None)),
            "Augment list should include no augment."
        );
        assert!(
            attacks.contains(&Attack(Coord::from(C), NorthWest, None)),
            "Augment list should include no augment."
        );
        assert!(
            !attacks.contains(&Attack(Coord::from(C), NorthEast, Some(0),),),
            "Augment list should not include non-augment cards."
        );
        assert!(
            !attacks.contains(&Attack(Coord::from(C), NorthWest, Some(0),),),
            "Augment list should not include non-augment cards."
        );
    }

    #[test]
    fn no_available_defences_in_empty_field() {
        let game = GameState::new();

        let defences = available_defences(&game);
        assert_eq!(
            defences.len(),
            0,
            "There should be no available defences in an empty game."
        );
    }

    #[test]
    fn no_available_defences_with_empty_hand() {
        let mut game = GameState::new();

        game.add_to_field_ready_current(C, Regular(0));
        game.add_to_field_ready(TRA, Regular(0), Player2);

        let new_state = game.act(Attack(Coord::from(C), NorthEast, None));

        let defences = available_defences(&new_state);
        assert_eq!(
            defences.len(),
            1,
            "There should be one defence option with an empty hand."
        );
        assert!(
            defences.contains(&Defend(None, Vec::new())),
            "The only option available should be no defence augment."
        );
    }

    #[test]
    fn available_defences_provides_augments() {
        let mut game = GameState::new();

        game.add_to_field_ready_current(C, Regular(0));
        game.add_to_field_ready(TRA, Regular(0), Player2);

        game.add_to_hand(Regular(0), Player2);
        game.add_to_hand(SmallAugment, Player2);
        game.add_to_hand(SmallAugment, Player2);
        game.add_to_hand(LargeAugment, Player2);

        let new_state = game.act(Attack(Coord::from(C), NorthEast, None));

        let defences = available_defences(&new_state);
        assert_eq!(
            defences.len(),
            4,
            "There should be four defence option with three augments in hand."
        );
        assert!(
            defences.contains(&Defend(None, Vec::new())),
            "The only option available should be no defence augment."
        );
        assert!(
            !defences.contains(&Defend(Some(0), Vec::new())),
            "Should not be able to defend with non-augment cards."
        );
    }

    #[test]
    fn available_defences_combines_discards() {
        let mut game = GameState::new();

        game.add_to_field_ready_current(TCD, Regular(0));
        game.add_to_field_ready(TLR, SmallRoyal, Player2);
        game.add_to_field_ready(TRR, LargeRoyal, Player2);

        game.add_to_hand(Regular(0), Player2);
        game.add_to_hand(SmallAugment, Player2);

        let new_state = game.clone().act(Attack(Coord::from(TCD), NorthWest, None));

        let defences = available_defences(&new_state);
        assert_eq!(
            defences.len(),
            3,
            "A small royal with one augment and one discard\
             in hand should have three available defences."
        );

        let new_state = game.clone().act(Attack(Coord::from(TCD), NorthEast, None));

        let defences = available_defences(&new_state);
        assert_eq!(
            defences.len(),
            1,
            "A large royal with one augment and one discard\
             in hand should have one available defence."
        );

        game.add_to_hand(Regular(0), Player2);
        game.add_to_hand(SmallAugment, Player2);

        let new_state = game.clone().act(Attack(Coord::from(TCD), NorthWest, None));

        let defences = available_defences(&new_state);
        assert_eq!(
            defences.len(),
            2 * 3 + // Two small augments with three other discards
        4, // No augment
            "A small royal with two augments and two discards\
             in hand should have ten available defences."
        );

        let new_state = game.clone().act(Attack(Coord::from(TCD), NorthEast, None));

        let defences = available_defences(&new_state);
        assert_eq!(
            defences.len(),
            2 * 3 + // Two small augments with three discard combinations
        6, // No augment, six discard combinations
            "A large royal with two augments and two discards\
             in hand should have twelve available defences."
        );
    }

    #[test]
    fn no_available_ends_while_not_turn() {
        let mut game = GameState::new();

        game.add_to_field_ready_current(C, Regular(0));
        game.add_to_field_ready(TRA, Regular(0), Player2);

        let new_state = game.act(Attack(Coord::from(C), NorthEast, None));

        let ends = available_ends(&new_state);
        assert_eq!(
            ends.len(),
            0,
            "Player should not be able to end turn while defending."
        );
    }

    #[test]
    fn available_ends_while_turn() {
        let game = GameState::new();

        let ends = available_ends(&game);
        assert_eq!(
            ends.len(),
            1,
            "Player should be able to end turn while on turn."
        );
    }

    #[test]
    fn available_ends_requires_discard_for_overage() {
        let mut game = GameState::new();

        for _ in 0..8 {
            game.add_to_hand_current(Regular(0));
        }

        let ends = available_ends(&game);
        assert_eq!(
            ends.len(),
            8,
            "Player should be able to discard any card for overage."
        );

        game.add_to_hand_current(Regular(0));

        let ends = available_ends(&game);
        assert_eq!(
            ends.len(),
            36, // 9 choose 2
            "Player should be able to discard any two cards for overage."
        );
    }

    #[test]
    fn all_available_actions_only_end_in_empty_game() {
        let game = GameState::new();

        let actions = all_available_actions(&game);
        assert_eq!(
            actions.len(),
            1,
            "Player should only be able to end turn in an empty game."
        );
        assert!(
            actions.contains(&End(Vec::new())),
            "Player should only be able to end turn in an empty game."
        );
    }

    #[test]
    fn all_available_actions_sums_actions() {
        let mut game = GameState::new();

        game.add_to_hand_current(Regular(0));
        game.add_to_hand_current(SmallRoyal);
        game.add_to_hand_current(SmallAugment);
        game.add_to_field_ready_current(C, Regular(0));
        game.add_to_field_ready(TRA, Regular(0), Player2);

        let actions = all_available_actions(&game);
        assert_eq!(
            actions.len(),
            7 + // Play regular card
            7 + // Play augment card
            2 + // Play augment card
            1 + // Move card
            1 * 2 + // Attack with card, with and without augment
            1, // End
            "Sum of actions does not total as expected."
        );

        let new_state = game.act(Attack(Coord::from(C), NorthEast, None));

        let actions = all_available_actions(&new_state);
        assert_eq!(
            actions.len(),
            1,
            "There should be one defence option with an empty hand."
        );
    }
}
