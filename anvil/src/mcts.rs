extern crate rand;

use {Action, GameState, PlayerID, TerminalState};
use available_actions::all_available_actions;

use rand::{thread_rng, Rng};

impl TerminalState {
    pub fn is_non_terminal(&self) -> bool {
        match self {
            &TerminalState::NonTerminal => true,
            _ => false,
        }
    }
}

type INode = usize;
type Reward = f32;

#[derive(Debug)]
struct Node {
    plays: u32,
    reward: Reward,
    parent: Option<INode>,
    edges: Vec<Action>,
    children: Vec<Option<INode>>,
}

impl Node {
    fn new(parent: Option<INode>, state: &GameState) -> Node {
        let available_actions = all_available_actions(state);
        let available_actions_count = available_actions.len();
        Node {
            plays: 0,
            reward: 0.,
            parent: parent,
            edges: available_actions,
            children: vec![None; available_actions_count],
        }
    }

    fn is_terminal(&self) -> bool {
        self.edges.len() == 0
    }

    fn is_fully_expanded(&self) -> bool {
        self.children.iter().all(|child| child.is_some())
    }
}

pub struct Tree {
    nodes: Vec<Node>,
    initial_state: GameState,
}

impl Tree {
    pub fn new(state: GameState) -> Tree {
        let mut tree = Tree {
            nodes: Vec::new(),
            initial_state: state,
        };
        tree.nodes.push(Node::new(None, &tree.initial_state));
        tree
    }

    pub fn run(&mut self) -> Action {
        for _ in 0..5000 {
            let (selected_node, state) = self.tree_policy();
            let reward = Self::default_policy(state).to_reward(self.initial_state.player_to_act);
            self.backup(selected_node, reward);
        }
        let best_child_index = self.best_child(0);
        self.nodes[0].edges[best_child_index].clone()
    }

    fn tree_policy(&mut self) -> (INode, GameState) {
        let mut current_node_index = 0;
        let mut current_state = self.initial_state.clone();
        while !self.nodes[current_node_index].is_terminal() {
            if !self.nodes[current_node_index].is_fully_expanded() {
                return self.expand(current_node_index, current_state);
            } else {
                let best_child_index = self.best_child(current_node_index);
                let best_action = self.nodes[current_node_index].edges[best_child_index].clone();
                current_node_index =
                    self.nodes[current_node_index].children[best_child_index].unwrap();
                current_state = current_state.act(best_action);
            }
        }
        (current_node_index, current_state)
    }

    fn expand(&mut self, node_index: INode, state: GameState) -> (INode, GameState) {
        let ((random_index, random_action), _) = self.nodes[node_index].edges
                .iter()
                .cloned()
                .enumerate()
                .zip(&self.nodes[node_index].children)
                .filter(|&(_, child)| child.is_none())
                .next() // Make random
                .unwrap();
        let new_state = state.act(random_action);
        let new_child = Node::new(Some(node_index), &new_state);
        self.nodes.push(new_child);
        let new_index = self.nodes.len() - 1;
        self.nodes[node_index].children[random_index] = Some(new_index);
        (new_index, new_state)
    }

    fn uct(current: &Node, child: &Node) -> f32 {
        let constant = 1f32 / 2f32.sqrt();
        child.reward / child.plays as f32
            + constant * (2f32 * (current.plays as f32).ln() / child.plays as f32).sqrt()
    }

    fn best_child(&self, node_index: INode) -> usize {
        self.nodes[node_index]
            .children
            .iter()
            .filter_map(|&child| child)
            .enumerate()
            .fold((0, 0f32), |(winning_index, max), (index, child)| {
                let score = Tree::uct(&self.nodes[node_index], &self.nodes[child]);
                if score > max {
                    (index, score)
                } else {
                    (winning_index, max)
                }
            })
            .0
    }

    fn default_policy(mut state: GameState) -> TerminalState {
        let mut rng = thread_rng();
        while state.terminal_state().is_non_terminal() {
            let available_actions = all_available_actions(&state);
            let random_action = rng.choose(&available_actions).unwrap();
            state = state.act(random_action.clone());
        }
        state.terminal_state()
    }

    fn backup(&mut self, start_node: INode, reward: Reward) {
        let mut node = Some(start_node);
        while let Some(node_index) = node {
            let node_ref = &mut self.nodes[node_index];
            node_ref.plays += 1;
            node_ref.reward += reward;
            node = node_ref.parent;
        }
    }
}

impl TerminalState {
    fn to_reward(&self, reference_player: PlayerID) -> Reward {
        match *self {
            TerminalState::Winner(player) => if player == reference_player {
                1.
            } else {
                0.
            },
            TerminalState::Draw => 0.5,
            TerminalState::NonTerminal => panic!("No reward for non-terminal state."),
        }
    }
}
