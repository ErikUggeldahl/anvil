use self::Visibility::*;

use {AttackState, Coord, DiscardPile, Field, FieldSpace, FieldSpaceOwner, FieldSpaceState,
     FieldSpaceType, GameState, Player, PlayerID};
use PlayerID::*;

use std::collections::HashMap;
use std::marker::Sized;

pub enum Visibility<T, U> {
    Visible(T),
    Hidden(U),
}

trait Hide<HiddenType>
where
    Self: Sized + Clone,
{
    fn show(self) -> Visibility<Self, HiddenType> {
        Visible(self)
    }

    fn hide(self) -> Visibility<Self, HiddenType>;
}

trait HideForPlayer<HiddenType> {
    fn hide(self, PlayerID) -> HiddenType;
}

pub struct HiddenFieldSpace {
    pub owner: FieldSpaceOwner,
    pub kind: FieldSpaceType,
}

pub type VisibleFieldSpace = Visibility<FieldSpace, HiddenFieldSpace>;

impl Hide<HiddenFieldSpace> for FieldSpace {
    fn hide(self) -> VisibleFieldSpace {
        Hidden(HiddenFieldSpace {
            owner: self.owner,
            kind: self.kind,
        })
    }
}

pub type VisibleField = HashMap<Coord, VisibleFieldSpace>;

impl HideForPlayer<VisibleField> for Field {
    fn hide(self, player: PlayerID) -> VisibleField {
        self.into_iter()
            .map(|(coord, space)| match space.state {
                FieldSpaceState::FaceDown(_) if space.owner != Some(player) => {
                    (coord, space.hide())
                }
                _ => (coord, space.show()),
            })
            .collect::<_>()
    }
}

pub type HiddenHand = usize;
pub type HiddenDeck = usize;

pub struct HiddenPlayer {
    pub id: PlayerID,
    pub hand: HiddenHand,
    pub deck: HiddenDeck,
    pub discard_pile: DiscardPile,
}

pub type VisiblePlayer = Visibility<Player, HiddenPlayer>;

impl Hide<HiddenPlayer> for Player {
    fn hide(self) -> VisiblePlayer {
        Hidden(HiddenPlayer {
            id: self.id,
            hand: self.hand.len(),
            deck: self.deck.len(),
            discard_pile: self.discard_pile,
        })
    }
}

pub struct HiddenAttackState {
    pub attacker: Coord,
    pub defender: Coord,
}

pub type VisibleAttackState = Visibility<AttackState, HiddenAttackState>;

impl HideForPlayer<VisibleAttackState> for AttackState {
    fn hide(self, player: PlayerID) -> VisibleAttackState {
        match self.attack_augment {
            Some(ref card) if card.owner != player => Hidden(HiddenAttackState {
                attacker: self.attacker,
                defender: self.defender,
            }),
            _ => Visible(self),
        }
    }
}

pub struct VisibleGameState {
    pub players_turn: PlayerID,
    pub player_to_act: PlayerID,
    pub field: VisibleField,
    pub attack_state: Option<VisibleAttackState>,
    pub player1: VisiblePlayer,
    pub player2: VisiblePlayer,
}

impl HideForPlayer<VisibleGameState> for GameState {
    fn hide(self, player: PlayerID) -> VisibleGameState {
        VisibleGameState {
            players_turn: self.players_turn,
            player_to_act: self.player_to_act,
            field: self.field.hide(player),
            attack_state: self.attack_state
                .map(|attack_state| attack_state.hide(player)),
            player1: if player == Player1 {
                self.player1.show()
            } else {
                self.player1.hide()
            },
            player2: if player == Player2 {
                self.player2.show()
            } else {
                self.player2.hide()
            },
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use Card;
    use CardType::*;
    use FieldSpaceState::*;
    use test_util::ReadableCoord::*;

    impl<T, U> Visibility<T, U> {
        fn is_visible(&self) -> bool {
            match self {
                &Visible(_) => true,
                _ => false,
            }
        }

        fn is_hidden(&self) -> bool {
            !self.is_visible()
        }
    }

    #[test]
    fn hide_field_reveals_empty_spaces() {
        let game = GameState::new();
        let visible_field = game.hide(Player1).field;
        assert!(
            visible_field
                .into_iter()
                .all(|(_, space)| space.is_visible()),
            "All spaces on an empty board should be visible."
        );
    }

    #[test]
    fn hide_field_hides_opponent_face_down_spaces() {
        let mut game = GameState::new();

        game.add_to_field(TLR, FaceDown(Card::new(SmallRoyal, Player2)));
        game.add_to_field(TLD, FaceDown(Card::new(Regular(0), Player2)));

        let visible_field = game.clone().hide(Player1).field;
        assert!(
            visible_field[&Coord::from(TLR)].is_hidden(),
            "Opponent's face down royal should be hidden."
        );
        assert!(
            visible_field[&Coord::from(TLD)].is_hidden(),
            "Opponent's face down defence card should be hidden."
        );

        let visible_field = game.hide(Player2).field;
        assert!(
            visible_field[&Coord::from(TLR)].is_visible(),
            "Opponent's face down royal should be visible to themselves."
        );
        assert!(
            visible_field[&Coord::from(TLD)].is_visible(),
            "Opponent's face down defence card should be visible to themselves."
        );
    }

    #[test]
    fn hide_field_reveals_opponent_face_up_spaces() {
        let mut game = GameState::new();

        game.add_to_field(TLR, Ready(Card::new(SmallRoyal, Player2)));
        game.add_to_field(TCD, Ready(Card::new(Regular(0), Player2)));
        game.add_to_field(TLA, Ready(Card::new(Regular(0), Player2)));

        let visible_field = game.hide(Player1).field;
        assert!(
            visible_field[&Coord::from(TLR)].is_visible(),
            "Opponent's face up rotal card should be visible."
        );
        assert!(
            visible_field[&Coord::from(TCD)].is_visible(),
            "Opponent's face up defence card should be visible."
        );
        assert!(
            visible_field[&Coord::from(TLA)].is_visible(),
            "Opponent's attack card should be visible."
        );
    }

    #[test]
    fn hide_field_reveals_owned_face_down_spaces() {
        let mut game = GameState::new();

        game.add_to_field(MLR, FaceDown(Card::new(SmallRoyal, Player1)));
        game.add_to_field(MCD, FaceDown(Card::new(Regular(0), Player1)));

        let visible_field = game.clone().hide(Player1).field;
        assert!(
            visible_field[&Coord::from(MLR)].is_visible(),
            "Players's face down royal should be visible to themselves."
        );
        assert!(
            visible_field[&Coord::from(MCD)].is_visible(),
            "Players's face down defence should be visible to themselves."
        );

        let visible_field = game.hide(Player2).field;
        assert!(
            visible_field[&Coord::from(MLR)].is_hidden(),
            "Players's face down royal should be hidden to opponent."
        );
        assert!(
            visible_field[&Coord::from(MCD)].is_hidden(),
            "Players's face down defence should be hidden to opponent."
        );
    }

    #[test]
    fn hide_player_hides_opponent_hand_and_deck() {
        let mut game = GameState::new();

        game.add_to_hand(Regular(0), Player2);
        game.add_to_hand(Regular(0), Player2);

        let visible_player = game.hide(Player1).player2;
        assert!(
            match visible_player {
                Hidden(player) => player.hand == 2 && player.deck == 26,
                _ => panic!("Opponent should be hidden."),
            },
            "Opponent's hand and deck counts are incorrectly represented."
        );
    }

    #[test]
    fn hide_player_reveals_owned_hand_and_deck() {
        let mut game = GameState::new();

        game.add_to_hand(Regular(0), Player1);
        game.add_to_hand(Regular(0), Player1);

        let visible_player = game.hide(Player1).player1;
        assert!(
            match visible_player {
                Visible(player) => player.hand.len() == 2 && player.deck.len() == 26,
                _ => panic!("Players's hand and deck should be visible."),
            },
            "Players's hand and deck counts are incorrect."
        );
    }

    #[test]
    fn hide_player_reveals_opponent_discard_pile() {
        let mut game = GameState::new();

        game.player2
            .discard_pile
            .push(Card::new(Regular(0), Player2));

        let visible_player = game.hide(Player1).player2;
        assert!(
            match visible_player {
                Hidden(player) => player
                    .discard_pile
                    .contains(&Card::new(Regular(0), Player2)),
                _ => panic!("Opponent should be hidden."),
            },
            "Opponent's discard pile did not contain expected card."
        );
    }

    #[test]
    fn hide_attack_state_does_not_alter_none_attack_or_none_augment() {
        let mut game = GameState::new();

        let visible_attack_state = game.clone().hide(Player1).attack_state;
        assert!(
            visible_attack_state.is_none(),
            "Empty board should have no attack state."
        );

        game.attack_state = Some(AttackState {
            attacker: Coord::from(C),
            defender: Coord::from(TLA),
            attack_augment: None,
        });

        let visible_attack_state = game.hide(Player1).attack_state;
        assert!(
            match visible_attack_state {
                Some(attack_state) => match attack_state {
                    Visible(attack_state) => attack_state.attack_augment.is_none(),
                    _ => panic!("Opponent's attack state should be visible without augments."),
                },
                None => panic!("Attack state should not be None after attack."),
            },
            "Attack augment should be none when no augments are played."
        );
    }

    #[test]
    fn hide_attack_state_hides_opponent_attack_state() {
        let mut game = GameState::new();

        game.attack_state = Some(AttackState {
            attacker: Coord::from(C),
            defender: Coord::from(TLA),
            attack_augment: Some(Card::new(SmallAugment, Player2)),
        });

        let visible_attack_state = game.hide(Player1).attack_state;
        assert!(
            match visible_attack_state {
                Some(attack_state) => match attack_state {
                    Hidden(attack_state) => {
                        attack_state.attacker == Coord::from(C) &&
                            attack_state.defender == Coord::from(TLA)
                    }
                    Visible(_) => panic!("Opponent's attack state should be hidden with augments."),
                },
                None => panic!("Attack state should not be None after attack."),
            },
            "Attack augment did not carry values after visibility change."
        );
    }

    #[test]
    fn hide_attack_state_reveals_owned_attack_state() {
        let mut game = GameState::new();

        game.attack_state = Some(AttackState {
            attacker: Coord::from(C),
            defender: Coord::from(TLA),
            attack_augment: Some(Card::new(SmallAugment, Player1)),
        });

        let visible_attack_state = game.hide(Player1).attack_state;
        assert!(
            match visible_attack_state {
                Some(attack_state) => match attack_state {
                    Visible(attack_state) => {
                        attack_state.attack_augment == Some(Card::new(SmallAugment, Player1))
                    }
                    Hidden(_) => panic!("Players's attack state should be visible to themselves."),
                },
                None => panic!("Attack state should not be None after attack."),
            },
            "Attack augment did not carry card after visibility change."
        );
    }
}
