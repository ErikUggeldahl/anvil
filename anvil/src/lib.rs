pub mod action;
pub mod available_actions;
pub mod mcts;
pub mod test_util;

extern crate rand;

use action::Action;
use available_actions::all_available_actions;

use action::Action::*;
use self::CardType::*;
use self::Direction::*;
use self::FieldSpaceState::*;
use self::FieldSpaceType::*;
use self::PlayerID::*;
use self::TerminalState::*;

use rand::{thread_rng, Rng};
use std::collections::HashMap;
use std::ops;

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum PlayerID {
    Player1,
    Player2,
}

type Rank = u8;

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Augment {
    NoAugment = 0,
    Small = 2,
    Large = 3,
}

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum Cost {
    None = 0,
    Small = 1,
    Large = 2,
}

#[derive(Clone, Copy)]
pub enum CardType {
    Regular(Rank),
    SmallAugment,
    LargeAugment,
    SmallRoyal,
    LargeRoyal,
}

#[derive(Clone, Debug, PartialEq)]
pub struct Card {
    pub owner: PlayerID,
    pub rank: Rank,
    pub augment: Augment,
    pub cost: Cost,
}

impl Card {
    pub fn new(card_type: CardType, owner: PlayerID) -> Card {
        match card_type {
            Regular(rank) => Card {
                owner,
                rank,
                augment: Augment::NoAugment,
                cost: Cost::None,
            },
            SmallAugment => Card {
                owner,
                rank: 1,
                augment: Augment::Small,
                cost: Cost::None,
            },
            LargeAugment => Card {
                owner,
                rank: 2,
                augment: Augment::Large,
                cost: Cost::None,
            },
            SmallRoyal => Card {
                owner,
                rank: 3,
                augment: Augment::NoAugment,
                cost: Cost::Small,
            },
            LargeRoyal => Card {
                owner,
                rank: 4,
                augment: Augment::NoAugment,
                cost: Cost::Large,
            },
        }
    }

    fn is_royal(&self) -> bool {
        self.cost != Cost::None
    }
}

type FieldSpaceOwner = Option<PlayerID>;

#[derive(Clone, Debug, PartialEq)]
pub enum FieldSpaceState {
    Empty,
    FaceDown(Card),
    Ready(Card),
    Moved(Card),
    Attacked(Card),
}

#[derive(Clone, Copy, PartialEq)]
pub enum FieldSpaceType {
    Neutral,
    Advance,
    Defence,
    Royal,
}

impl FieldSpaceType {
    fn is_royal(&self) -> bool {
        *self == Royal
    }
}

#[derive(Clone)]
pub struct FieldSpace {
    pub owner: FieldSpaceOwner,
    pub state: FieldSpaceState,
    pub kind: FieldSpaceType,
}

impl FieldSpace {
    fn get_card(&self) -> &Card {
        match self.state {
            FaceDown(ref card) | Ready(ref card) | Moved(ref card) | Attacked(ref card) => card,
            Empty => panic!("Cannot get a card for an empty space."),
        }
    }

    fn get_card_mut(&mut self) -> Card {
        let state = std::mem::replace(&mut self.state, Empty);
        match state {
            FaceDown(card) | Ready(card) | Moved(card) | Attacked(card) => card,
            Empty => panic!("Cannot get a card for an empty space."),
        }
    }
}

#[derive(Clone, Copy, Debug, Eq, Hash, PartialEq)]
pub struct Coord(pub i8, pub i8, pub i8);

impl Coord {
    fn next(&self, direction: Direction) -> Coord {
        let delta = match direction {
            NorthWest => Coord(0, 1, -1),
            NorthEast => Coord(1, 0, -1),
            West => Coord(-1, 1, 0),
            East => Coord(1, -1, 0),
            SouthWest => Coord(-1, 0, 1),
            SouthEast => Coord(0, -1, 1),
        };

        *self + delta
    }
}

impl ops::Add<Coord> for Coord {
    type Output = Coord;

    fn add(self, Coord(x2, y2, z2): Coord) -> Coord {
        let Coord(x1, y1, z1) = self;
        Coord(x1 + x2, y1 + y2, z1 + z2)
    }
}

#[derive(Clone, Copy, Debug, Hash, Eq, PartialEq)]
pub enum Direction {
    NorthWest,
    NorthEast,
    West,
    East,
    SouthWest,
    SouthEast,
}

type Field = HashMap<Coord, FieldSpace>;

fn create_field() -> Field {
    fn coord_to_field_space(coord: &Coord) -> FieldSpace {
        let owner = match *coord {
            Coord(_, _, z) if z > 0 => Some(Player1),
            Coord(_, _, z) if z < 0 => Some(Player2),
            _ => None,
        };

        let kind = match *coord {
            Coord(_, _, z) if z == 0 => Neutral,
            Coord(_, _, z) if z.abs() == 1 => Advance,
            Coord(_, _, z) if z.abs() == 2 => Defence,
            Coord(_, _, z) if z.abs() == 3 => Royal,
            _ => panic!("Invalid coordinate."),
        };

        FieldSpace {
            owner,
            state: Empty,
            kind,
        }
    }

    fn all_field_coords() -> Vec<Coord> {
        let mut all_coords = Vec::<Coord>::new();
        let (x_size, y_size, z_size) = (2, 2, 3);
        for x in -x_size..x_size + 1 {
            for y in -y_size..y_size + 1 {
                for z in -z_size..z_size + 1 {
                    if x + y + z == 0 {
                        all_coords.push(Coord(x, y, z));
                    }
                }
            }
        }
        all_coords
    }

    let all_coords = all_field_coords();
    all_coords
        .iter()
        .map(|coord| (*coord, coord_to_field_space(coord)))
        .collect::<Field>()
}

fn full_royals(field: &Field, player: PlayerID) -> bool {
    let coords = match player {
        Player1 => [Coord(-1, -2, 3), Coord(-2, -1, 3)],
        Player2 => [Coord(1, 2, -3), Coord(2, 1, -3)],
    };

    coords.into_iter().all(|coord| match field[coord].state {
        Empty => false,
        _ => true,
    })
}

type Hand = Vec<Card>;

type Deck = Vec<Card>;

type DiscardPile = Vec<Card>;

pub type CardIndex = usize;

fn create_deck(owner: PlayerID) -> Deck {
    let card_counts = vec![
        (Regular(1), 8),
        (Regular(2), 2),
        (SmallAugment, 4),
        (LargeAugment, 4),
        (SmallRoyal, 4),
        (LargeRoyal, 4),
    ];

    card_counts
        .iter()
        .flat_map(|&(card_type, count)| vec![Card::new(card_type, owner); count])
        .collect()
}

#[derive(Clone)]
pub struct Player {
    pub id: PlayerID,
    pub hand: Hand,
    pub deck: Deck,
    pub discard_pile: DiscardPile,
}

impl Player {
    fn new(id: PlayerID) -> Player {
        Player {
            id,
            deck: create_deck(id),
            hand: Hand::new(),
            discard_pile: DiscardPile::new(),
        }
    }
}

#[derive(Clone)]
pub struct AttackState {
    pub attacker: Coord,
    pub defender: Coord,
    pub attack_augment: Option<Card>,
}

#[derive(Debug, PartialEq)]
pub enum TerminalState {
    Winner(PlayerID),
    Draw,
    NonTerminal,
}

#[derive(Clone)]
pub struct GameState {
    pub players_turn: PlayerID,
    pub player_to_act: PlayerID,
    pub field: Field,
    pub attack_state: Option<AttackState>,
    pub player1: Player,
    pub player2: Player,
}

impl GameState {
    pub fn new() -> GameState {
        GameState {
            players_turn: Player1,
            player_to_act: Player1,
            field: create_field(),
            attack_state: None,
            player1: Player::new(Player1),
            player2: Player::new(Player2),
        }
    }

    pub fn init(mut self) -> GameState {
        let mut rng = thread_rng();
        rng.shuffle(&mut self.player1.deck);
        rng.shuffle(&mut self.player2.deck);

        for _ in 0..7 {
            self.player1.hand.push(self.player1.deck.pop().unwrap());
            self.player2.hand.push(self.player2.deck.pop().unwrap());
        }
        self
    }

    pub fn terminal_state(&self) -> TerminalState {
        let end_spaces = vec![
            &self.field[&Coord(-1, -2, 3)],
            &self.field[&Coord(-2, -1, 3)],
            &self.field[&Coord(1, 2, -3)],
            &self.field[&Coord(2, 1, -3)],
        ];
        for space in &end_spaces {
            match space.state {
                Ready(ref card) | Moved(ref card) if Some(card.owner) != space.owner => {
                    return Winner(card.owner)
                }
                _ => (),
            }
        }
        if self.player1.deck.len() == 0 && self.player2.deck.len() == 0
            && all_available_actions(&self) == vec![End(Vec::new())]
            && all_available_actions(&self.clone().act(End(Vec::new()))) == vec![End(Vec::new())]
        {
            Draw
        } else {
            NonTerminal
        }
    }

    pub fn current_player(&self) -> &Player {
        match self.player_to_act {
            Player1 => &self.player1,
            Player2 => &self.player2,
        }
    }

    pub fn other_player(&self) -> &Player {
        match self.player_to_act {
            Player1 => &self.player2,
            Player2 => &self.player1,
        }
    }

    fn current_player_mut(&mut self) -> &mut Player {
        match self.player_to_act {
            Player1 => &mut self.player1,
            Player2 => &mut self.player2,
        }
    }

    fn other_player_mut(&mut self) -> &mut Player {
        match self.player_to_act {
            Player1 => &mut self.player2,
            Player2 => &mut self.player1,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use test_util::ReadableCoord::*;

    #[test]
    fn create_field_has_correct_count() {
        let field = create_field();
        assert_eq!(field.keys().count(), 23);
    }

    #[test]
    fn create_field_has_valid_coord_values() {
        let field = create_field();
        for &Coord(x, y, z) in field.keys() {
            assert_eq!(x + y + z, 0);
        }
    }

    #[test]
    fn terminal_state_is_non_terminal_with_no_victory() {
        let mut game = GameState::new();
        game.add_to_field_ready(TLD, Regular(0), Player1);
        game.add_to_field_ready(TRD, Regular(0), Player1);
        game.add_to_field_ready(MLR, SmallRoyal, Player1);
        game.add_to_field_ready(MLD, Regular(0), Player2);
        game.add_to_field_ready(MRD, Regular(0), Player2);
        game.add_to_field_ready(TLR, SmallRoyal, Player2);

        assert_eq!(
            game.terminal_state(),
            NonTerminal,
            "There should be no winner without cards in the royal row."
        );
    }

    #[test]
    fn terminal_state_has_winner_with_victory() {
        let mut game = GameState::new();
        game.add_to_field_ready(TLR, Regular(0), Player1);
        assert_eq!(
            game.terminal_state(),
            Winner(Player1),
            "Player 1 should be in a winning position."
        );

        let mut game = GameState::new();
        game.add_to_field(TRR, Moved(Card::new(Regular(0), Player1)));
        assert_eq!(
            game.terminal_state(),
            Winner(Player1),
            "Player 1 should be in a winning position."
        );

        let mut game = GameState::new();
        game.add_to_field_ready(MLR, Regular(0), Player2);
        assert_eq!(
            game.terminal_state(),
            Winner(Player2),
            "Player 2 should be in a winning position."
        );

        let mut game = GameState::new();
        game.add_to_field(MRR, Moved(Card::new(Regular(0), Player2)));
        assert_eq!(
            game.terminal_state(),
            Winner(Player2),
            "Player 2 should be in a winning position."
        );
    }

    #[test]
    fn terminal_state_is_draw_without_available_actions() {
        let mut game = GameState::new();
        game.player1.deck.clear();
        game.player2.deck.clear();
        game.add_to_field(MLR, FaceDown(Card::new(SmallRoyal, Player1)));
        game.add_to_field(MCD, FaceDown(Card::new(Regular(0), Player1)));
        game.add_to_field(TLR, Ready(Card::new(SmallRoyal, Player2)));
        game.add_to_field(TCD, Ready(Card::new(Regular(0), Player2)));

        assert_eq!(
            game.terminal_state(),
            Draw,
            "Game should draw with no actions available."
        );
        assert_eq!(all_available_actions(&game), vec![Action::End(Vec::new())]);
        game = game.act(Action::End(Vec::new()));
        assert_eq!(all_available_actions(&game), vec![Action::End(Vec::new())]);
    }
}
