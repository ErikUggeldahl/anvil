use {Card, CardType, Coord, FieldSpaceState, GameState, PlayerID};
use FieldSpaceState::*;
use PlayerID::*;

use self::ReadableCoord::*;

impl GameState {
    pub fn add_to_hand(&mut self, card: CardType, player: PlayerID) {
        let player = match player {
            Player1 => &mut self.player1,
            Player2 => &mut self.player2,
        };

        player.hand.push(Card::new(card, player.id));
    }

    pub fn add_to_hand_current(&mut self, card: CardType) {
        let current_player = self.current_player_mut().id;
        self.add_to_hand(card, current_player);
    }

    pub fn add_to_field<T: Into<Coord>>(&mut self, coord: T, state: FieldSpaceState) {
        self.field.get_mut(&coord.into()).unwrap().state = state;
    }

    pub fn add_to_field_ready<T: Into<Coord>>(
        &mut self,
        coord: T,
        card: CardType,
        player: PlayerID,
    ) {
        self.field.get_mut(&coord.into()).unwrap().state = Ready(Card::new(card, player));
    }

    pub fn add_to_field_ready_current<T: Into<Coord>>(&mut self, coord: T, card: CardType) {
        let current_player = self.current_player_mut().id;
        self.add_to_field_ready(coord.into(), card, current_player);
    }
}

// Coordinates encoded as M(y)/T(heir) L(eft)/C(enter)/R(ight) R(oyal)/D(efence)/A(ttack)
#[allow(dead_code)]
#[derive(Clone)]
pub enum ReadableCoord {
    MLR,
    MRR,
    MLD,
    MCD,
    MRD,
    MLLA,
    MLA,
    MRA,
    MRRA,
    RRC,
    RC,
    C,
    LC,
    LLC,
    TLLA,
    TLA,
    TRA,
    TRRA,
    TLD,
    TCD,
    TRD,
    TLR,
    TRR,
}

impl From<ReadableCoord> for Coord {
    fn from(coord: ReadableCoord) -> Self {
        match coord {
            MLR => Coord(-2, -1, 3),
            MRR => Coord(-1, -2, 3),
            MLD => Coord(-2, 0, 2),
            MCD => Coord(-1, -1, 2),
            MRD => Coord(0, -2, 2),
            MLLA => Coord(-2, 1, 1),
            MLA => Coord(-1, 0, 1),
            MRA => Coord(0, -1, 1),
            MRRA => Coord(1, -2, 1),
            RRC => Coord(-2, 2, 0),
            RC => Coord(-1, 1, 0),
            C => Coord(0, 0, 0),
            LC => Coord(1, -1, 0),
            LLC => Coord(2, -2, 0),
            TLLA => Coord(-1, 2, -1),
            TLA => Coord(0, 1, -1),
            TRA => Coord(1, 0, -1),
            TRRA => Coord(2, -1, -1),
            TLD => Coord(0, 2, -2),
            TCD => Coord(1, 1, -2),
            TRD => Coord(2, 0, -2),
            TLR => Coord(1, 2, -3),
            TRR => Coord(2, 1, -3),
        }
    }
}
